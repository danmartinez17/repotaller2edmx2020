var productosObtenidos;

function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      //console.table(JSON.parse(request.responseText).value);
      productosObtenidos = request.responseText;
      procesarProductos();

    }

  };
  request.open("GET", url, true);
  request.send();

}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaProductName = document.createElement("td");
    columnaProductName.innerText = JSONProductos.value[i].ProductName;
    var columnaunitPrice = document.createElement("td");
    columnaunitPrice.innerText = JSONProductos.value[i].unitPrice;
    var columnaUnitsInStock = document.createElement("td");
    columnaUnitsInStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaProductName);
    nuevaFila.appendChild(columnaunitPrice);
    nuevaFila.appendChild(columnaUnitsInStock);

    tbody.appendChild(nuevaFila);

  }
  //fin del for

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
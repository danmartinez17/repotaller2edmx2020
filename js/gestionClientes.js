var clientesObtenidos;

function getClientes() {
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Mexico'";
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();

    }

  };
  request.open("GET", url, true);
  request.send();

}

function procesarClientes() {
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var ext = ".png";
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONClientes.value[0].ContactName);
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaContactName = document.createElement("td");
    columnaContactName.innerText = JSONClientes.value[i].ContactName;
    var columnaCity = document.createElement("td");
    columnaCity.innerText = JSONClientes.value[i].City;
    //var columnaCountry = document.createElement("td");
    //columnaCountry.innerText=JSONClientes.value[i].Country;
    var columnabandera = document.createElement("td");
    var imgBander = document.createElement("img");
    imgBander.classList.add("flag");

    if (JSONClientes.value[i].Country == "UK") {
      imgBander.src = rutaBandera + "United-Kingdom" + ext;
    } else {
      imgBander.src = rutaBandera + JSONClientes.value[i].Country + ext;
    }
    columnabandera.appendChild(imgBander);

    //https://www.countries-ofthe-world.com/flags-normal/flag-of-Madagascar.png

    nuevaFila.appendChild(columnaContactName);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnabandera);

    tbody.appendChild(nuevaFila);

  }
  //fin del for

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}